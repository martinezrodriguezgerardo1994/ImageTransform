#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->progressBar->setVisible(false);

    imgRes = QString("270x405,360x540,540x810,720x1080,1080x1620").split(',');
    iconRes = QString("48x48,72x72,96x96,144x144,192x192").split(',');
    menuIconRes = QString("24x24,36x36,48x48,72x72").split(',');

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_rbImages_clicked()
{
    ui->listWidget->clear();
    ui->listWidget->addItems(imgRes);
}

void MainWindow::on_rbIcons_clicked()
{
    ui->listWidget->clear();
    ui->listWidget->addItems(iconRes);
}

void MainWindow::on_rbMenuIcons_clicked()
{
    ui->listWidget->clear();
    ui->listWidget->addItems(menuIconRes);
}

void MainWindow::on_pbDelete_clicked()
{
    delete ui->listWidget->currentItem();
}

void MainWindow::on_pbAdd_clicked()
{
    ui->listWidget->addItem(ui->leWidth->text()+'x'+ui->leHeight->text());
}

void MainWindow::on_qualitySlider_sliderMoved(int position)
{
    ui->qualitySlider->setToolTip(QString("%1").arg(position));
}

void MainWindow::on_pbStart_clicked()
{
    QVector<QSize> scales;
    for (int i = 0; i < ui->listWidget->count(); ++i){
        QStringList tmp = ui->listWidget->item(i)->text().split('x');
        scales.append(QSize(tmp[0].toInt(), tmp[1].toInt()));
    }
    QString input = QFileDialog::getExistingDirectory(0,"Carpeta de fotos a escalar","C:/Users/Usuario/Desktop",
                                                      QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
    QString output = QFileDialog::getExistingDirectory(0,"Salida de salida","C:/Users/Usuario/Desktop",
                                                       QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
    ui->progressBar->setVisible(true);
    scalator(input, scales, output);
}

void MainWindow::scalator(QString sourceFolder, QVector<QSize> dimensions, QString outputFolder)
{
    QDir source(sourceFolder), output(outputFolder);

    foreach (QSize s, dimensions) {
        output.mkdir(QString("%1x%2").arg(s.height()).arg(s.width()));
    }
    QFileInfoList info = source.entryInfoList(QDir::Files | QDir::NoSymLinks | QDir::NoDotAndDotDot |  QDir::Readable);
    int count = 0;
    foreach (QFileInfo i, info) {
        ++count;
        QImage currentImage(i.absoluteFilePath());
        if(currentImage.format() != QImage::Format_Invalid){
            foreach (QSize d, dimensions) {
                QString photoNumber = QString("v40").append(QString::number(count)).append(".jpg");
              currentImage.scaled(d,Qt::KeepAspectRatio, Qt::SmoothTransformation).save(QString("%1/%2x%3/" +
                                                                                                photoNumber)
                                                              .arg(outputFolder).arg(d.height()).arg(d.width())
                                                              , "jpg", 92);
            }
        }
    }
}
