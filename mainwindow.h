#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDir>
#include <QFileDialog>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_rbImages_clicked();

    void on_rbIcons_clicked();

    void on_rbMenuIcons_clicked();

    void on_pbDelete_clicked();

    void on_pbAdd_clicked();

    void on_qualitySlider_sliderMoved(int position);

    void on_pbStart_clicked();

private:
    void scalator(QString sourceFolder, QVector<QSize> dimensions, QString outputFolder);

    Ui::MainWindow *ui;
    QStringList imgRes, iconRes, menuIconRes;
};

#endif // MAINWINDOW_H
